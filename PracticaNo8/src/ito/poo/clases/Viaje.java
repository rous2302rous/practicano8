package ito.poo.clases;

import java.time.LocalDate;

public class Viaje {

	private int numeroviaje;  
	private String ciudad;
	private String destino;
	private String dirección; 
	private LocalDate fechaviaje; 
	private LocalDate fecharegreso;
	private String descripcióncarga; 
	private float montoviaje;
	
	public Viaje(int numeroviaje, 
			String ciudad, 
			String destino, 
			String dirección, 
			LocalDate fechaviaje,
			LocalDate fecharegreso, 
			String descripcióncarga, 
			float montoviaje) {
		super();
		this.numeroviaje = numeroviaje;
		this.ciudad = ciudad;
		this.destino = destino;
		this.dirección = dirección;
		this.fechaviaje = fechaviaje;
		this.fecharegreso = fecharegreso;
		this.descripcióncarga = descripcióncarga;
		this.montoviaje = montoviaje;
	}

	public int getNumeroviaje() {
		return numeroviaje;
	}

	public void setNumeroviaje(int numeroviaje) {
		this.numeroviaje = numeroviaje;
	}

	public String getCiudad() {
		return ciudad;
	}

	public void setCiudad(String ciudad) {
		this.ciudad = ciudad;
	}

	public String getDestino() {
		return destino;
	}

	public void setDestino(String destino) {
		this.destino = destino;
	}

	public String getDirección() {
		return dirección;
	}

	public void setDirección(String dirección) {
		this.dirección = dirección;
	}

	public LocalDate getFechaviaje() {
		return fechaviaje;
	}

	public void setFechaviaje(LocalDate fechaviaje) {
		this.fechaviaje = fechaviaje;
	}

	public LocalDate getFecharegreso() {
		return fecharegreso;
	}

	public void setFecharegreso(LocalDate fecharegreso) {
		this.fecharegreso = fecharegreso;
	}

	public String getDescripcióncarga() {
		return descripcióncarga;
	}

	public void setDescripcióncarga(String descripcióncarga) {
		this.descripcióncarga = descripcióncarga;
	}

	public float getMontoviaje() {
		return montoviaje;
	}

	public void setMontoviaje(float montoviaje) {
		this.montoviaje = montoviaje;
	}

	@Override
	public String toString() {
		return "Viaje [numeroviaje=" + numeroviaje + ", " + (ciudad != null ? "ciudad=" + ciudad + ", " : "")
				+ (destino != null ? "destino=" + destino + ", " : "")
				+ (dirección != null ? "dirección=" + dirección + ", " : "")
				+ (fechaviaje != null ? "fechaviaje=" + fechaviaje + ", " : "")
				+ (fecharegreso != null ? "fecharegreso=" + fecharegreso + ", " : "")
				+ (descripcióncarga != null ? "descripcióncarga=" + descripcióncarga + ", " : "") + "montoviaje="
				+ montoviaje + "]";
	}
	
	
	
}
