package ito.poo.clases;

import java.time.LocalDate;
import java.util.ArrayList;

public class Veiculo {

	private int numerovehiculo; 
	private String marca; 
	private String modelo; 
	private int maximapermitido; 
	private LocalDate fechaadquisicion;
	private ArrayList<Viaje> viajesrealizados;
	
	public Veiculo(int numerovehiculo, 
			String marca, 
			String modelo, 
			int maximapermitido,
			LocalDate fechaadquisicion
			) {
		super();
		this.viajesrealizados = new ArrayList<Viaje>();
		this.numerovehiculo = numerovehiculo;
		this.marca = marca;
		this.modelo = modelo;
		this.maximapermitido = maximapermitido;
		this.fechaadquisicion = fechaadquisicion;
		
	}
	
	public void add(Viaje viaje) {
		
		viajesrealizados.add(viaje);
		
	}
	
	public boolean cancelar(int numero) {
		
		boolean band=false;
		for(int i=0; i<viajesrealizados.size(); i++) {
			
			if(numero == viajesrealizados.get(i).getNumeroviaje()) {
				viajesrealizados.remove(i);
				band = true;
			}
			
		}
		if(band)
			return true;
		else
			return false;
	}
	
	public int getNumerovehiculo() {
		return numerovehiculo;
	}

	public void setNumerovehiculo(int numerovehiculo) {
		this.numerovehiculo = numerovehiculo;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getModelo() {
		return modelo;
	}

	public void setModelo(String modelo) {
		this.modelo = modelo;
	}

	public int getMaximapermitido() {
		return maximapermitido;
	}

	public void setMaximapermitido(int maximapermitido) {
		this.maximapermitido = maximapermitido;
	}

	public LocalDate getFechaadquisicion() {
		return fechaadquisicion;
	}

	public void setFechaadquisicion(LocalDate fechaadquisicion) {
		this.fechaadquisicion = fechaadquisicion;
	}

	public ArrayList<Viaje> getViajesrealizados() {
		return viajesrealizados;
	}

	@Override
	public String toString() {
		return "Transporte [numerovehiculo=" + numerovehiculo + ", " + (marca != null ? "marca=" + marca + ", " : "")
				+ (modelo != null ? "modelo=" + modelo + ", " : "") + "maximapermitido=" + maximapermitido + ", "
				+ (fechaadquisicion != null ? "fechaadquisicion=" + fechaadquisicion + ", " : "")
				+ (viajesrealizados != null ? "viajesrealizados=" + viajesrealizados : "") + "]";
	}
	
}
