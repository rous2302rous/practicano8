package ito.poo.app;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;

import ito.poo.clases.Veiculo;
import ito.poo.clases.Viaje;

public class Transporte {
	
	public static Veiculo v = new Veiculo(123, "audi", "2020", 150, LocalDate.of(2021, 11, 27));

	private static ArrayList<Veiculo> datos;
	
	public Transporte() {
		
		this.datos = new ArrayList<Veiculo>();
		
	}

	public static void add(Veiculo veiculo) {
		
		datos.add(veiculo);
		
	}
	
    public static void asignacion(int numero, Viaje viaje) {
		boolean band=false;
		for(int i=0; i<datos.size(); i++) {
			
			if(numero == datos.get(i).getNumerovehiculo()) {
				v.add(viaje);
				band = true;
			}
		}
		
		if(band)
			System.out.println("Se Asigno el viaje al vehiculo");
		else
			System.out.println("No se encontro el numero del vehiculo");
	}
    
    public static void canselacion(int numerov) {
    	if(v.cancelar(numerov))
    		System.out.println("Se canselo el viaje");
    	else
    		System.out.println("No se encontro el numero de viaje");
	}
	
    public static void menu() {
    	Scanner tec = new Scanner(System.in);
    	int op=0;
    	do{
    		System.out.println("Inserte su opcion:\n 1)Agregar vehiculo\n 2)Agregar viaje a un vehiculo\n 3)Canselar viaje\n 4)Salir");
        	op = tec.nextInt();
    		switch(op) {
    		
    			case 1:
    				int numero, maxima;
    				String marca, modelo;
    				System.out.println("Ingresa el numero del vehiculo");
    				numero = tec.nextInt();
    				System.out.println("Ingresa el marca");
    				marca = tec.next();
    				System.out.println("Ingresa el modelo");
    				modelo = tec.next();
    				System.out.println("Ingresa el maxima carga permitida");
    				maxima = tec.nextInt();
    				add(new Veiculo(numero, marca, modelo, maxima, LocalDate.of(2021, 11, 27)));
    				break;
    			case 2:
    				int nviaje;
    				float monto;
    				String ciudad, destino, direccion, descripcion;
    				System.out.println("Ingresa el numero del vehiculo");
    				numero = tec.nextInt();
    				System.out.println("Ingresa el numero de viaje");
    				nviaje = tec.nextInt();
    				System.out.println("Ingresa la ciudad");
    				ciudad = tec.next();
    				System.out.println("Ingresa el destino");
    				destino = tec.next();
    				System.out.println("Ingresa el direccion");
    				direccion = tec.next();
    				System.out.println("Ingresa la descripcion de la carga");
    				descripcion = tec.next();
    				System.out.println("Ingresa el monto de viaje");
    				monto = tec.nextInt();
    				asignacion(numero, new Viaje(nviaje, ciudad, destino, direccion, LocalDate.of(2021, 11, 27), LocalDate.of(2021, 11, 28), descripcion, monto));
    				break;
    			case 3:
    				System.out.println("Ingresa el numero de viaje");
    				numero = tec.nextInt();
    				canselacion(numero);
    				break;
    			case 4:
    				System.out.println("Adios");
    				break;
    			default:
    				System.out.println("Este numero no existe");
    		}
    		
    	}while(op!=4);
    }
    
    public static void main(String[] args) {
    	
    	menu();
    	
    }
    
}
